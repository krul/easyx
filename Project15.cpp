#include<graphics.h>
#include<conio.h>
#include<Windows.h>
#include<stdlib.h>
#include<math.h>
#define ballnum 14
int ballx[ballnum], bally[ballnum];
int main()
{
	int i,j;
	int vx[ballnum] ;
	int vy[ballnum] ;
	initgraph(1280, 640);
	BeginBatchDraw();
	for (i = 0; i < ballnum; i++)
	{
		while (ballx[i] < 30)
		{
			ballx[i] = rand() % 1200;
		}
		while (bally[i] < 30)
		{
			bally[i] = rand() % 600;
		}
		vx[i] = 1;
		vy[i] = 1; 
	}
	while (true)
	{
		setcolor(YELLOW);
		setfillcolor(GREEN);
		for (i = 0; i < ballnum; i++)
		{
			fillcircle(ballx[i], bally[i], 20);
		}
		FlushBatchDraw();
		setcolor(BLACK);
		setfillcolor(BLACK);
		for (i = 0; i < ballnum; i++)
		{
			fillcircle(ballx[i], bally[i], 20);
			ballx[i] += vx[i];
			bally[i] += vy[i];
			if (ballx[i] == 1260 || ballx[i] == 20)
				vx[i] = -vx[i];
			if (bally[i] == 620 || bally[i] == 20)
				vy[i] = -vy[i];
		}
			int mindistances[ballnum][2];
			for (i = 0; i < ballnum; i++)
			{
				mindistances[i][0] = 999999;
				mindistances[i][1] = -1;
			}
			for (i = 0; i < ballnum; i++)
			{
				for (j = 0; j < ballnum; j++)
				{
					if (i != j)
					{
						int dist;
						dist = (ballx[i] - ballx[j])*(ballx[i] - ballx[j])
							+ (bally[i] - bally[j])*(bally[i] - bally[j]);
						if (dist < mindistances[i][0])
						{
							mindistances[i][0] = dist;
							mindistances[i][1] = j;
						}
					}
				}
			}
			for (i = 0; i < ballnum; i++)
			{
				if (mindistances[i][0] <= 1600)
				{
					j = mindistances[i][1];
					int temp;
					temp = vx[i];
					vx[i] = vx[j];
					vx[j] = temp;
					temp = vy[i];
					vy[i] = vy[j];
					vy[j] = temp;
					mindistances[j][0] = 999999;
					mindistances[j][1] = -1;
				}
			}
	}
	EndBatchDraw();
	closegraph();
	return 0;
}
